#ifndef MATRIX_H
#define MATRIX_H

#include <stdint.h>

/**
 * Create a structure of type matrix_t.
 */
typedef struct matrix_t {
  int rows;
  int columns;
  int64_t ** data;
} matrix_t;

/**
 * Initialise an object of type sparse matrix with dimension n x m.
 *
 * @param The number of rows in the matrix.
 * @param The number of columns in the matrix.
 *
 * @return The pointer to the matrix.
 */
matrix_t * matrix_init(int n, int m);

/**
 * Free the memory of the sparse matrix.
 *
 * @param The pointer to the matrix.
 */
void matrix_free(matrix_t * matrix);

/**
 * Set the value of any element in the sparse matrix to a value.
 *
 * @param The pointer to the matrix.
 * @param The x co-ordinate of the element.
 * @param The y co-ordinate of the element.
 * @param The value that is to be set in the matrix.
 */
void matrix_set(matrix_t * matrix, int x, int y, int64_t val);

/**
 * Get the value from the sparse matrix at co-ordinates x, y.
 *
 * @param The pointer to the matrix.
 * @param The x co-ordinate of the element.
 * @param The y co-ordinate of the element.
 *
 * @return The value that is to be retrieved.
 */
int64_t matrix_get(matrix_t * matrix, int x, int y);

/**
 * Print a matrix.
 *
 * @param The pointer to the matrix.
 */
 void matrix_print(matrix_t * matrix);

/**
 * Add a scalar value to each element in a matrix.
 *
 * @param The pointer to the matrix.
 * @param The scalar value.
 *
 * @return The pointer to the matrix.
 */
matrix_t * matrix_add_scalar(matrix_t * matrix, int scalar);

/**
 * Multiply a scalar value to each element in a matrix.
 *
 * @param The pointer to the matrix.
 * @param The scalar value.
 *
 * @return The pointer to the matrix.
 */
matrix_t * matrix_mult_scalar(matrix_t * matrix, int scalar);

/**
 * Add one matrix to another.
 *
 * @param The pointer to the first matrix.
 * @param The pointer to the second matrix.
 *
 * @return The pointer to the resulting matrix, or NULL if not possible.
 */
matrix_t * matrix_add(matrix_t * m1, matrix_t * m2);

/**
 * Multiply one matrix by another.
 *
 * @param The pointer to the first matrix.
 * @param The pointer to the second matrix.
 *
 * @return The pointer to the resulting matrix, or NULL if not possible.
 */
matrix_t * matrix_mul(matrix_t * m1, matrix_t * m2);

/**
 * Save a matrix to a file.
 *
 * @param The pointer to a matrix.
 * @param The pointer to a file.
 *
 * @return If successful, 0, otherwise an error code.
 */
int matrix_save(matrix_t * m1, char const * file);

/**
 * Load a matrix from a file.
 *
 * @param The pointer to a file.
 *
 * @return The pointer to the matrix, or NULL if not possible.
 */
matrix_t * matrix_load(char const * file);

#endif

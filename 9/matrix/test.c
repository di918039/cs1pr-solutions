#include <stdio.h>
#include "matrix.h"

int main(int argc, char ** argv) {

  /* Initialise a matrix */
  matrix_t * m1 = matrix_init(5, 6);

  //////////////////////////////////////////////////////////////////////////////

  /* Store a value in any location in the matrix */
  for (int i = 0; i < 5; i++) {
    for (int j = 0; j < 6; j++) {
      matrix_set(m1, j, i, i + j);
    }
  }

  // Print m1
  printf("m1\n");
  matrix_print(m1);
  printf("\n");

  //////////////////////////////////////////////////////////////////////////////

  /* Add a scalar value to m1 */
  matrix_t * m1_add_scalar = matrix_add_scalar(m1, 5);
  printf("m1_add_scalar\n");
  matrix_print(m1_add_scalar);
  printf("\n");

  //////////////////////////////////////////////////////////////////////////////

  /* Multiply a scalar value to m1 */
  matrix_t * m1_mult_scalar = matrix_mult_scalar(m1, 5);
  printf("m1_mult_scalar\n");
  matrix_print(m1_mult_scalar);
  printf("\n");

  //////////////////////////////////////////////////////////////////////////////

  /* Add one matrix to another */
  if (matrix_add(m1, m1_add_scalar) != NULL) {
    matrix_t * m1_add = matrix_add(m1, m1_add_scalar);

    printf("m1_add\n");
    matrix_print(m1_add);
    printf("\n");

    matrix_free(m1_add);
  }

  // Add two different sized matrices
  matrix_t * m2 = matrix_init(4, 6);
  matrix_t * m3 = matrix_init(5, 7);

  if (matrix_add(m1, m2) != NULL) {
    matrix_t * m2_add = matrix_add(m1, m2);

    printf("m2_add\n");
    matrix_print(m2_add);
    printf("\n");

    matrix_free(m2_add);
  }

  if (matrix_add(m1, m3) != NULL) {
    matrix_t * m3_add = matrix_add(m1, m3);

    printf("m3_add\n");
    matrix_print(m3_add);
    printf("\n");

    matrix_free(m3_add);
  }

  //////////////////////////////////////////////////////////////////////////////

  /* Multiply one matrix by another */
  matrix_t * m4 = matrix_init(6, 3);

  for (int i = 0; i < 6; i++) {
    for (int j = 0; j < 3; j++) {
      matrix_set(m4, j, i, i + j);
    }
  }

  printf("m4\n");
  matrix_print(m4);
  printf("\n");

  if (matrix_mul(m1, m4) != NULL) {
    matrix_t * m1_mul = matrix_mul(m1, m4);

    printf("m1_mul\n");
    matrix_print(m1_mul);
    printf("\n");

    matrix_free(m1_mul);
  }

  // Multiply two different sized matrices
  if (matrix_mul(m1, m2) != NULL) {
    matrix_t * m2_mul = matrix_mul(m1, m2);

    printf("m2_mul\n");
    matrix_print(m2_mul);
    printf("\n");

    matrix_free(m2_mul);
  }

  if (matrix_mul(m1, m3) != NULL) {
    matrix_t * m3_mul = matrix_mul(m1, m3);

    printf("m3_mul\n");
    matrix_print(m3_mul);
    printf("\n");

    matrix_free(m3_mul);
  }

  //////////////////////////////////////////////////////////////////////////////

  /* Save matrix */
  int saved = matrix_save(m1, "matrix.txt");

  if (saved == 0) {
    printf("m1 saved successfully\n\n");
  } else {
    printf("error saving m1\n\n");
  }

  //////////////////////////////////////////////////////////////////////////////

  /* Load matrix */
  matrix_t * m5 = matrix_load("matrix.txt");
  printf("m5\n");
  matrix_print(m5);
  printf("\n");

  //////////////////////////////////////////////////////////////////////////////

  /* Free matrices */
  matrix_free(m1);
  matrix_free(m1_add_scalar);
  matrix_free(m1_mult_scalar);
  matrix_free(m2);
  matrix_free(m3);
  matrix_free(m4);
  matrix_free(m5);

  return 0;
}

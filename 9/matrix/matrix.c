#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include "matrix.h"

matrix_t * matrix_init(int n, int m) {

  // Allocate memory for the matrix
  matrix_t * matrix = malloc(sizeof(matrix_t));

  // Assign values to each struct members
  matrix->rows = n;
  matrix->columns = m;
  matrix->data = malloc(sizeof(int64_t *) * n);

  // Set each element in the matrix to 0
  for (int i = 0; i < n; i++) {
    matrix->data[i] = calloc(m, sizeof(int64_t));
  }

  return matrix;
}

void matrix_free(matrix_t * matrix) {
  for (int i = 0; i < matrix->rows; i++) {
    free(matrix->data[i]);
  }
  free(matrix->data);
  free(matrix);
}

void matrix_set(matrix_t * matrix, int x, int y, int64_t val) {
  matrix->data[y][x] = val;
}

int64_t matrix_get(matrix_t * matrix, int x, int y) {
  return matrix->data[y][x];
}

void matrix_print(matrix_t * matrix) {

  for (int y = 0; y < matrix->rows; y++) {
    for (int x = 0; x < matrix->columns; x++) {

      if (x == matrix->columns - 1) {
        printf("%ld", matrix_get(matrix, x, y));
      } else {
        printf("%ld, ", matrix_get(matrix, x, y));
      }

    }
    printf("\n");
  }

}

matrix_t * matrix_add_scalar(matrix_t * matrix, int scalar) {

  matrix_t * out = matrix_init(matrix->rows, matrix->columns);

  for (int y = 0; y < matrix->rows; y++) {
    for (int x = 0; x < matrix->columns; x++) {
      out->data[y][x] = matrix->data[y][x] + scalar;
    }
  }

  return out;
}

matrix_t * matrix_mult_scalar(matrix_t * matrix, int scalar) {

  matrix_t * out = matrix_init(matrix->rows, matrix->columns);

  for (int y = 0; y < matrix->rows; y++) {
    for (int x = 0; x < matrix->columns; x++) {
      out->data[y][x] = matrix->data[y][x] * scalar;
    }
  }

  return out;
}

matrix_t * matrix_add(matrix_t * m1, matrix_t * m2) {

  // If the number of rows and columns are not the same, raise an error
  if (m1->rows != m2->rows || m1->columns != m2->columns) {
    return NULL;
  }

  matrix_t * out = matrix_init(m1->rows, m1->columns);

  for (int y = 0; y < out->rows; y++) {
    for (int x = 0; x < out->columns; x++) {
      out->data[y][x] = m1->data[y][x] + m2->data[y][x];
    }
  }

  return out;
}

matrix_t * matrix_mul(matrix_t * m1, matrix_t * m2) {

  // If the number of rows of the first matrix is not equal to the number of
  // columns of the second matrix
  if (m1->columns != m2->rows) {
    return NULL;
  }

  matrix_t * out = matrix_init(m1->rows, m2->columns);

  for (int y = 0; y < out->rows; y++) {
    for (int x = 0; x < out->columns; x++) {

      for (int i = 0; i < m1->columns ; i++) {
        out->data[y][x] += m1->data[y][i] * m2->data[i][x];
      }

    }
  }

  return out;
}

int matrix_save(matrix_t * m1, char const * file) {

  FILE * filePointer = fopen(file, "w");

  // If there is an error opening the file, return the error code
  if (filePointer == NULL) {
    return errno;
  }

  for (int y = 0; y < m1->rows; y++) {
    for (int x = 0; x < m1->columns; x++) {

      if (x == m1->columns - 1) {
        fprintf(filePointer, "%ld", matrix_get(m1, x, y));
      } else {
        fprintf(filePointer, "%ld,", matrix_get(m1, x, y));
      }

    }
    fprintf(filePointer, "\n");
  }

  fclose(filePointer);
  return 0;
}

matrix_t * matrix_load(char const * file) {

  FILE * filePointer = fopen(file, "r");

  // If there is an error opening the file, return NULL
  if (filePointer == NULL) {
    return NULL;
  }

  int rows = 0;
  int columns = 0;
  char line[1024];
  char * token = NULL;

  // Get number of rows
  for (char c = getc(filePointer); c != EOF; c = getc(filePointer)) {
    if (c == '\n') {
      rows++;
    }
  }

  rewind(filePointer);

  // Get number of columns
  for (char c = getc(filePointer); c != '\n'; c = getc(filePointer)) {
    if (c == ',') {
      columns++;
    }
  }
  columns++;

  rewind(filePointer);

  matrix_t * out = matrix_init(rows, columns);

  int rowIndex = 0;

  // Transfer each element from the file into the matrix
  while (fgets(line, sizeof(line), filePointer) != NULL && rowIndex < rows) {
    int columnIndex = 0;
    for (token = strtok(line, ","); token != NULL && columnIndex < columns; token = strtok(NULL, ",")) {
      out->data[rowIndex][columnIndex++] = atoi(token);
    }
    rowIndex++;
  }

  fclose(filePointer);
  return out;
}

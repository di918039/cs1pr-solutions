#ifndef QSORT_H
#define QSORT_H

/**
 * Swaps the values held by two integer variables.
 *
 * @param The first integer.
 * @param The second integer.
 *
 * @return Exit status code of the function.
 */
int swap(int * a, int * b);

/**
 * Recursively applies the quick sort algorithm.
 *
 * @param Array of numbers that are to be sorted.
 * @param Index of the smaller element.
 * @param Index of the greater element.
 *
 * @return Exit status code of the function.
 */
int quicksort(int * array, int low, int high);

/**
 * Places pivot in correct position, and moves the other elements accordingly.
 *
 * @param Array of numbers that are to be sorted.
 * @param Index of the smaller element.
 * @param Index of the greater element.
 *
 * @param Exit status code of the function.
 */
int partition(int * array, int low, int high);

/**
 * Sorts an integer array using the quick sort algorithm.
 *
 * @param The integer array to be sorted.
 * @param The size of the integer array.
 *
 * @return Exit status code of the function.
 */
int mqsort(int * array, int size);

#endif

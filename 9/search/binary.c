#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "qsort.h"

/**
 * Finds the index of the first or last occurence of the value in an array, using binary search.
 *
 * @param The target value that is to be searched for in the array.
 * @param The number of elements in the array.
 * @param The pointer to the array.
 * @param Value to determine whether to find the first or last occurence of the value in the array.
 *
 * @return The index of the first occurence or the last occurence.
 */
int search(int v, int n, int * N, int searchFirst);

/**
 * Main entrypoint to the program.
 *
 * @param Number of arguments passed to the program.
 * @param Character array of command line arguments.
 *
 * @return Exit status code of the program.
 */
int main(int argc, char ** argv) {

  // If the correct number of command line arguments are not passed, return an
  // error code
  if (argc != 4 && argc != 5) {
    return 1;
  }

  // Assign command line arguments to variables
  int m = atoi(argv[1]);
  int n = atoi(argv[2]);
  int s = atoi(argv[3]);
  int debug = 0;

  // If there are five command line arguments, the fifth is the debug mode
  if (argc == 5) {
    debug = atoi(argv[4]);
  }

  // Initiliase an integer array of n random integers, each with a value less
  // than m
  srand(time(NULL));
  int N[n];
  for (int i = 0; i < n; i++) {
    N[i] = rand() % m;
  }

  // Initialise an integer array of s random integers, each with a value less
  // than m
  int S[s];
  for (int i = 0; i < s; i++) {
    S[i] = rand() % m;
  }

  // If the program is being run in debug mode, print N
  if (debug != 0) {
    for (int i = 0; i < n; i++) {
      printf("%d ", N[i]);
    }
    printf("\n");
  }

  // Start the timer
  clock_t start = clock();

  // Sort the array N in ascending order
  mqsort(N, n);

  // Count the number of times each value from S appears in N
  for (int i = 0; i < s; i++) {
    int first = search(S[i], n, N, 1);
    int last = search(S[i], n, N, 0);
    int count = last - first + 1;

    // If the program is being run in debug mode, print each value from S and
    // the number of times it appears in N
    if (debug != 0) {
      printf("%d:%d\n", S[i], count);
    }
  }

  // Stop the timer and calculate the duration of the search
  clock_t end = clock();
  double runtime = ((double)(end - start) / CLOCKS_PER_SEC) * 1000;

  printf("%d,%d,%d,%g\n", m, n, s, runtime);

  return 0;
}

int search(int v, int n, int * N, int searchFirst) {

  int lower = 0;
  int upper = n - 1;
  int count = -1;

  // Repeat until there is one or zero elements
  while (lower <= upper) {
    int mid = (lower + upper) / 2;

    if (v == N[mid]) {
      count = mid;

      // Traverse to the left
      if (searchFirst) {
        upper = mid - 1;

      // Traverse to the right
      } else {
        lower = mid + 1;
      }

    } else if (v < N[mid]) {
      upper = mid - 1;
    } else {
      lower = mid + 1;
    }

  }

  return count;
}

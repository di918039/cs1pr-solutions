#!/bin/bash

gcc -O3 naive.c -o naive
gcc -O3 qsort.c binary.c -o binary

./naive 10 100 100
./naive 10 1000 100
./naive 10 10000 100
./naive 10 100000 100
echo Not done by the student, but shows that naive is slower
./naive 10 100000 30000

./binary 10 100 100
./binary 10 1000 100
./binary 10 10000 100
./binary 10 100000 100
./binary 10 100000 30000


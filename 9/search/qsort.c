#include "qsort.h"

int swap(int * a, int * b) {
  int temp = * b;
  * b = * a;
  * a = temp;
  return 0;
}

int quicksort(int * array, int lower, int upper) {

  int p;

  if (lower < upper) {
    p = partition(array, lower, upper);
    quicksort(array, lower, p - 1);
    quicksort(array, p + 1, upper);
  }

  return 0;
}

int partition(int * array, int lower, int upper) {

  int pivot = array[upper];
  int i = lower;

  for (int j = lower; j < upper; j++) {
    if (array[j] < pivot) {
      swap(&array[i], &array[j]);
      i++;
    }
  }

  swap(&array[i], &array[upper]);

  return i;
}

int mqsort(int * array, int size) {
  quicksort(array, 0, size - 1);
  return 0;
}

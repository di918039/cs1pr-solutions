#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/**
 * Counts the number of times a value appears in an array, using linear search.
 *
 * @param The target value that is to be searched for in the array.
 * @param The number of elements in the array.
 * @param The pointer to the array.
 *
 * @return The number of times the value appears in the given array.
 */
int search(int v, int n, int * N);

/**
 * Main entrypoint to the program.
 *
 * @param Number of arguments passed to the program.
 * @param Character array of command line arguments.
 *
 * @return Exit status code of the program.
 */
int main(int argc, char ** argv) {

  // If the correct number of command line arguments are not passed, return an
  // error code
  if (argc != 4 && argc != 5) {
    return 1;
  }

  // Assign command line arguments to variables
  int m = atoi(argv[1]);
  int n = atoi(argv[2]);
  int s = atoi(argv[3]);
  int debug = 0;

  // If there are five command line arguments, the fifth is the debug mode
  if (argc == 5) {
    debug = atoi(argv[4]);
  }

  // Initiliase an integer array of n random integers, each with a value less
  // than m
  srand(time(NULL));
  int N[n];
  for (int i = 0; i < n; i++) {
    N[i] = rand() % m;
  }

  // Initialise an integer array of s random integers, each with a value less
  // than m
  int S[s];
  for (int i = 0; i < s; i++) {
    S[i] = rand() % m;
  }

  // If the program is being run in debug mode, print N
  if (debug != 0) {
    for (int i = 0; i < n; i++) {
      printf("%d ", N[i]);
    }
    printf("\n");
  }

  // Start the timer
  clock_t start = clock();

  // Count the number of times each value from S appears in N
  for (int i = 0; i < s; i++) {
    int count = search(S[i], n, N);

    // If the program is being run in debug mode, print each value from S and
    // the number of times it appears in N
    if (debug != 0) {
      printf("%d:%d\n", S[i], count);
    }
  }

  // Stop the timer and calculate the duration of the search
  clock_t end = clock();
  double runtime = ((double)(end - start) / CLOCKS_PER_SEC) * 1000;

  printf("%d,%d,%d,%g\n", m, n, s, runtime);

  return 0;
}

int search(int v, int n, int * N) {
  int count = 0;

  for (int i = 0; i < n; i++) {
    if (v == N[i]) {
      count++;
    }
  }

  return count;
}

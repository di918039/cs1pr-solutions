#include <matrix.h>
#include <assert.h>
#include <stdio.h>

int main(){
  matrix_t * m = matrix_init(10, 10);
  assert(matrix_get(m,5,5) == 0);

  matrix_set(m, 5,5, 4711);
  int64_t val = matrix_get(m,5,5);
  printf("%lld\n", (long long unsigned) val);
  assert(val == 4711);
  matrix_print(m);
  return 0;
}

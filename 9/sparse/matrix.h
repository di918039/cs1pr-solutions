#include <stdint.h>

typedef struct{
  int pos; // -1 means end of the ragged array
  int64_t value;
} row_t;

struct matrix_t{
  int n;
  int m;

  row_t ** v;
};

typedef struct matrix_t matrix_t;

/* Initialize a sparse matrix of dimension n x m */
matrix_t * matrix_init(int n, int m);

/* Free the sparse matrix */
void matrix_free(matrix_t * matrix);
/* Set the value of the matrix on coordinates x, y to val */
void matrix_set(matrix_t * matrix, int x, int y, int64_t val);
/**
 Return the value of the matrix on coordinates x, y, return 0 if not set
*/
int64_t matrix_get(matrix_t * matrix, int x, int y);
void matrix_print(matrix_t * matrix);

#include <matrix.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

matrix_t * matrix_init(int n, int m){
    matrix_t * matrix = (matrix_t*) malloc(sizeof(matrix_t));
    matrix->n = n;
    matrix->m = m;
    matrix->v = malloc(sizeof(void*) * n);
    memset(matrix->v, 0, sizeof(void*) * n);

    return matrix;
}

void matrix_free(matrix_t * matrix){
  for(int i = 0; i < matrix->m; i++){
    if(matrix->v[i]) free(matrix->v[i]);
  }
  free(matrix->v);
  free(matrix);
}

void matrix_set(matrix_t * matrix, int x, int y, int64_t val){
  assert(matrix);
  assert(matrix->n > x && matrix->m > y);

  if (matrix->v[x] == NULL){
    row_t * new = malloc((y+1)*sizeof(row_t));
    new[0].pos = y;
    new[0].value = val;
    new[1].pos = -1;
    matrix->v[x] = new;
  }else{

  }
}

int64_t matrix_get(matrix_t * matrix, int x, int y){
  if(matrix->n > x && matrix->m > y){
    // inside the matrix
    if (matrix->v[x] == NULL){
      return 0;
    }
    // check if the y coordinate can be found in the array
    row_t * a = matrix->v[x];
    for(; a->pos != -1; a++){
      if(a->pos == y){
        return a->value;
      }
    }
  }
  // out of bounds
  return 0;
}

void matrix_print(matrix_t * matrix){
  for(int y = 0; y < matrix->m; y++){
    for(int x = 0; x < matrix->n; x++){
      int64_t val = matrix_get(matrix, y, x);
      if( x == 0){
        printf("%lld", val);
      }else{
        printf(", %lld", val);
      }
    }
    printf("\n");
  }
}

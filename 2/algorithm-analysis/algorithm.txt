The program outputs all prime numbers between 2 and n, it does this by discounting all 
values in the array which are indexed with a number which is a multiple of a number between 2 and n other than itself.
It discounts the value by replacing it as '0' in the array
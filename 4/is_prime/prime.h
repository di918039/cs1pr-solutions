#ifndef PRIME_H
#define PRIME_H

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

bool is_prime(int number);

#endif

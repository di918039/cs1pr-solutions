#include "prime.h"

int main(int argc, char *argv[])
{
  int number;
  /* check we have an argument to the main program */
  if(argc!=2){
    printf("%s:usage <number>\n",argv[0]);
    return 1; /* print error message and exit */
  }

  /* convert string to integer */
  number = atoi(argv[1]);

  /* call prime number calculator */
  if( is_prime(number) ){
    printf("%d is prime\n", number);
  }else{
    printf("%d is NOT prime\n", number);
  }
  return 0;
}


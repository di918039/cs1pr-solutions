#include <stdio.h>

#include "my_gcd_lib.h"


int main(int argc, char ** argv){
  int a = atoi(argv[1]);
  int b = atoi(argv[2]);
  printf("the gcd is: %d\n", gcd(a, b));

  return 0;

}

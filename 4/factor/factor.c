#include <stdio.h>
#include <string.h>
#include "my_math.h"

/**
*takes the user input and outputs its prime factors
*@returns 0
*/
//parameters - argv[1]: an integer
int main(int argc, char ** argv){
  int n = atoi(argv[1]);
  print_factor(n);
  return 0;
}

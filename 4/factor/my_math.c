#include "my_math.h"
#include <stdio.h>

/**
*checks if the in
*@returns integer 1 or 0 (is prime?)
*/
//parameters - n: an inputted integer
int is_prime(int n){
  int i, prime = 1;

  for (i = 2; i <= (n/2); i++) {
    if (n % i == 0) {
      prime = 0;
      break;
    }
  }

  return prime;
}

/**
*performs modulus operations on n until it finds one which is a factor
*a prime, prints the factor, and recursively repeats till all factors
*are printed
*@returns the recursive function print_factor with the new value
*/
//parameters - n: an inputted integer
int print_factor(int n){
	if(is_prime(n) == 1){
		printf("%d\n",n);
		return 0;
	}
	for(int i = 2;i<=(n/2);i++){
		if(n%i == 0){
			if(is_prime(i) == 1){
				printf("%d,",i);
				n = (n/i);
				return (print_factor(n));
			}
		}
	}
}

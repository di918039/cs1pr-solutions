#include "my_conversion.h"
#include <stdio.h>
/**
*converts the inputted string into it's respective integer
*@returns the integer value
*/
//parameters - str:the integer value in string format
int my_atoi(char * str){
	int sign = 1;
	int i = 0;
	char first = str[0];
	if (first == '-'){
		sign = -1;
		i = 1;
	}
	int res = 0;
	for (int j = i; str[j] != '\0'; j++){
		res = res * 10 + str[j] - '0';
	}
	res = res*sign;
	return res;
}

#include <stdio.h>
#include <string.h>
#include "my_conversion.h"
/**
*converts the inputs to integer values and outputs the sum
*@returns zero
*/
//parameters - argv[1]:an integer value
int main(int argc, char ** argv){
	argc++;
	int x = my_atoi(argv[1]);
	int y = my_atoi(argv[2]);
	printf("%d\n",(x+y));
	return 0;
}

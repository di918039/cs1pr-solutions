#include <stdbool.h>
#ifndef MY_MATH_H
#define MY_MATH_H
  /**
  * Checks number for primality.
  * Uses the AKS primality test to check if a given number is prime.
  *
  * @param number Number to be checked for primality.
  *
  * @return Boolean that states whether the given number is prime or not.
  */
  bool is_prime(int);
  /**
  * Prints all prime factors of a number in acending order.
  *
  * @param n Number for which the prime factors are printed.
  *
  * @return Exit status code of the program.
  */
  int print_factor(int);
#endif

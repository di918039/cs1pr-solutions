#include <stdio.h>
#include <stdbool.h>

bool is_prime(int number) {
  if (number == 2 || number == 3) {
    return true;
  }

  if (number % 2 == 0 || number % 3 == 0) {
    return false;
  }

  int i = 5;
  int w = 2;

  while (i * i <= number) {
    if (number % i == 0) {
      return false;
    }

    i = i + w;
    w = 6 - w;
  }

  return true;
}

int print_factor(int n) {
  int originalN = n;

  // While the number hasn't been divided by itself
  while (n > 1) {

    // If n hasn't been fully factorised, add a comma to separate the last
    // digit from the next one
    if (n != originalN) {
      printf(",");
    }

    // For all the candidates for prime factors
    for (int i = 2; i <= n; i++) {

      // If the counter, i, is prime and divides cleanly into the number n,
      // it is considered one of the prime factors and is printed
      // The number is then divided by this prime factor
      if (is_prime(i) && n % i == 0) {
        printf("%d", i);
        n = n / i;
        break;
      }

      // If the number is prime itself, there are no more prime factors to find,
      // so print n, and divide n by itself i.e. set n to 1
      if (is_prime(n)) {
        printf("%d", n);
        n = n / n;
        break;
      }
    }
  }

  // Add a new line
  printf("\n");

  return 0;
}

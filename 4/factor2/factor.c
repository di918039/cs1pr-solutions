#include <stdio.h>
#include <stdlib.h>
#include "my_math.h"

/**
* Main entrypoint to the program.
* Calls the print_factor function.
*
* @param argc Number of arguments passed to the program.
* @param argv Character array of positional parameters.
*
* @return Exit status code of the program.
*/
int main(int argc, char **argv) {
  int number = atoi(argv[1]);

  print_factor(number);

  return 0;
}

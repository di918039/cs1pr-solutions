#include <stdio.h>
#include "my_conversion.h"

/**
* Addition calculator.
* Takes in two numbers passed as positional parameters and prints the sum of them.
*
* @param argc Number of arguments passed to the program.
* @param argv Character array of positional parameters.
*
* @return Exit status code of the program.
*/
int main(int argc, char **argv) {
  int numberOne = my_atoi(argv[1]);
  int numberTwo = my_atoi(argv[2]);

  printf("%d\n", numberOne + numberTwo);

  return 0;
}

#include <string.h>

int raise_power(int, int);

int my_atoi(char* string) {
  // Length of the character array
  int numberOfDigits = (int)strlen(string);

  // Used to move each digit into the correct place
  int exponent = numberOfDigits - 1;

  // Initialise variable to hold the final result after converting all of the
  // digits from characters to integers
  int result = 0;

  // Index to get the (i - 1)th character
  int i = 0;

  // If the number is a negative, ignore the sign by increasi the index to point
  // to the next character, i.e. a number
  if (string[0] == '-') {
    i++;
    numberOfDigits--;
    exponent--;
  }

  // Convert each character of the character array into an integer
  // Subtract 48 from this integer and multiply the number with 10^exponent
  // Add this to the result variable
  for (int j = i; j <= numberOfDigits - 1; j++) {
    result = result + (((int)string[j]) - 48) * raise_power(10, exponent);
    exponent--;
  }

  // If the number was passed as a negative, make the result negative
  if (string[0] == '-') {
    result = result - (2 * result);
  }

  return result;
}

/**
* Raises a base to a given exponent.
*
* @param base The base number.
* @param exopnent The exponent that is applied to the base.
*
* @return The resulting number after the base is raised to the exponent.
*/
int raise_power(int base, int exponent) {
  int result = 1;

  while (exponent > 0) {
    result = result * base;
    exponent--;
  }

  return result;
}

#ifndef MY_CONVERSION_H
#define MY_CONVERSION_H
  /**
  * Converts character array to integer.
  * Converts the character array used to represent an integer into the
  * equivalent using an integer data type.
  *
  * @param string The character array used to hold the number.
  *
  * @return The number passed as a positional parameter in an integer data type.
  */
  int my_atoi(char*);
#endif

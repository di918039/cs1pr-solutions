#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
/**
*adds the inputted value to all positive numbers lower than it including zero
*@returns the sum of the values
*/
//parameters - n: integer
int sumOf(int n)
{
    if (n > 1)
        return n+sumOf(n-1);
    else
        return 1;
}


/**
*recieves the inputted number and finds the mean from the sum
*@returns 0
*/
//parameters - argv[1]: an integer
int main(int argc, char ** argv[]){
	int n = atoi(argv[1]);
	int sum = sumOf(n);
	float mean = (sum / (float)(n+1));
	printf("%d %.1f \n",sum,mean);
  return 0;
}

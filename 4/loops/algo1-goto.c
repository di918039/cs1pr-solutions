#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
/**
*adds the inputted value to all positive numbers lower than it including zero
*@returns zero
*/
//parameters - argv[1]:an integer value
int main(int argc, char ** argv[]){
	int n = atoi(argv[1]);
	int sum = 0;
	int i = 0;

	LOOP:
		sum = sum + i;
		i++;
		if (i<=n){
			goto LOOP;
		}

	float mean = (sum / (float)(n+1));
	printf("%d %.1f \n",sum,mean);
	return 0;
}

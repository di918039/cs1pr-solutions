#include <stdio.h>
#include <string.h>

/**
*checks every letter of the inputted word with the letter on the opposite
*side of the word, if all of them match, the word is a palindrome
*@returns decision (a 1 or 0)
*/
//parameters - j:a counting integer, num:string length,
//						 word:string , decision:whether the word is a palindrome
int checker(int j,int num,char * word,int decision){
	if (num == 0){
		return 0;
	}
	if (word[j] != word[num-j-1]){
		decision = 0;
		return 0;
	}else{
		j++;
		if (j<num){
			return (checker(j, num, word, decision));
		}else{
			return (decision);
		}
	}
}

/**
*checks every letter of the inputted word with the letter on the opposite
*side of the word, if all of them match, the word is a palindrome
*@returns zero
*/
//parameters - argv[1]:an integer value
int main(int argc, char ** argv[]){
	char * str = argv[1];
	int n = strlen(str);
	int palindrome = 1;
	if (checker(0, n, str, 1) == 0){
		palindrome = 0;
	}


	if(palindrome == 1){
		printf("palindrome\n");
	}else{
		printf("no palindrome\n");
	}
	return 0;
}

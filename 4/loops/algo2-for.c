#include <stdio.h>
#include <string.h>
/**
*checks every letter of the inputted word with the letter on the opposite
*side of the word, if all of them match, the word is a palindrome
*@returns zero
*/
//parameters - argv[1]:an integer value
int main(int argc, char ** argv[]){
	char * str = argv[1];
	int n = strlen(str);
	int palindrome = 1;
	int i;

	if (n == 0){
		palindrome = 0;
	}

	for(i=0; i<(n/2);i++){
		if(str[i] != str[n-i-1]){
			palindrome = 0;
			break;
		}
	}


	if(palindrome == 1){
		printf("palindrome\n");
	}else{
		printf("no palindrome\n");
	}
	return 0;
}

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

int recursivePalindromeChecker(int, char*, int, bool);

/**
 * Main entrypoint to the program.
 * Calls the recursivePalindromeChecker function.
 *
 * @param argc Number of arguments passed to the program.
 * @param argv Character array of positional parameters.
 *
 * @return Exit status code of the program.
 */
int main(int argc, char **argv) {
  char * input = argv[1];
  int length = strlen(input);
  bool palindrome = true;

  int i = 0;
  recursivePalindromeChecker(length, input, i, palindrome);

  return 0;
}

/**
 * Checking for palindrome.
 * Takes a string and checks if it is a valid palindrome, reads the same
 * forwards as it does backwards, using a for loop.
 *
 * @param length Number that states the length of the string.
 * @param input Character array of the string passed as a positional parameter.
 * @param counter Number to count how many times the function has been called.
 * @param palindrome Boolean that states whether or not the string is a palindrome.
 *
 * @return Exit status code of the program.
 */
int recursivePalindromeChecker(int length, char * input, int counter, bool palindrome) {
  if (counter == (length/2 + 1)) {
    if (palindrome) {
      printf("palindrome\n");
    } else {
      printf("no palindrome\n");
    }
    return counter;
  } else {
    if (input[counter] != input[length-counter-1]) {
      palindrome = false;
    }
    recursivePalindromeChecker(length, input, counter + 1, palindrome);
  }
  return 0;
}

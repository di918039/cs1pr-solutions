#include <stdio.h>
#include <string.h>
#include <stdbool.h>

/**
 * Checking for palindrome.
 * Takes a string and checks if it is a valid palindrome, reads the same
 * forwards as it does backwards, using a goto loop.
 *
 * @param argc Number of arguments passed to the program.
 * @param argv Character array of positional parameters.
 *
 * @return Exit status code of the program.
 */
int main(int argc, char **argv) {
  char * str = argv[1];
  int n = strlen(str);
  bool palindrome = true;

  int i = 0;
  loop:
  if (str[i] != str[n-i-1]) {
    palindrome = false;
  }
  i++;
  if (i < n/2) {
    goto loop;
  }

  if (palindrome) {
    printf("palindrome\n");
  } else {
    printf("no palindrome\n");
  }

  return 0;
}

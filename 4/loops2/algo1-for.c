#include <stdio.h>
#include <stdlib.h>

/**
 * Computing sum and mean.
 * Takes a positive integer, n, and calculates the sum of the numbers between 0
 * and n using a for loop, and also calculates the mean of this sum.
 *
 * @param argc Number of arguments passed to the program.
 * @param argv Character array of positional parameters.
 *
 * @return Exit status code of the program.
 */
int main(int argc, char **argv) {
  int number = atoi(argv[1]);
  int sum = 0;

  for (int i = 0; i <= number; i++) {
    sum = sum + i;
  }

  float mean = (float)sum/(number + 1);

  printf("%d %.1f\n", sum, mean);

  return 0;
}

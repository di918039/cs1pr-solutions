#include <stdio.h>
#include <stdlib.h>

int recursiveSumAndMean(int, int, int);

/**
 * Main entrypoint to the program.
 * Calls the recursiveSumAndMean function.
 *
 * @param argc Number of arguments passed to the program.
 * @param argv Character array of positional parameters.
 *
 * @return Exit status code of the program.
 */
int main(int argc, char **argv) {
  int number = atoi(argv[1]);
  int sum = 0;

  int i = 0;
  recursiveSumAndMean(number, sum, i);

  return 0;
}

/**
 * Computing sum and mean.
 * Takes a positive integer, number, and calculates the sum of the numbers
 * between 0 and number using recursion, and also calculates the mean of this
 * sum.
 *
 * @param number Number passed as a positional parameter to the program.
 * @param sum The sum of the numbers between 0 and number.
 * @param counter Number to count how many times the function has been called.
 *
 * @return Exit status code of the program.
 */
int recursiveSumAndMean(int number, int sum, int counter) {
  if (counter == number + 1) {
    printf("%d %.1f\n", sum, (float)sum/(counter));
    return counter;
  } else {
    sum = sum + counter;
    recursiveSumAndMean(number, sum, counter + 1);
  }

  return 0;
}

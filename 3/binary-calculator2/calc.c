#include <stdio.h>
#include <stdlib.h>

void decimalToBinary(int, int*);
void twosComplement(int*);
void addTwoBinaryNumbers(int*, int*, int*);

int main(int argc, char **argv) {
  // assign positional parameters to variables
  int firstDecimal = atoi(argv[1]);
  char operator = argv[2][0];
  int secondDecimal = atoi(argv[3]);

  // declare integer arrays of size 8 to hold the 8 bit binary numbers
  int firstBinary[8];
  int secondBinary[8];

  // convert the decimal numbers to their binary equivalents
  decimalToBinary(firstDecimal, firstBinary);
  decimalToBinary(secondDecimal, secondBinary);

  // if the decimal numbers are negative, convert the binary number
  // to represent the negative value using two's complement
  if (firstDecimal < 0) {
    twosComplement(firstBinary);
  }

  if (secondDecimal < 0) {
    twosComplement(secondBinary);
  }

  // declare integer array of size 8 to hold the result
  int result[8];

  // if operator is +, add the two binary numbers together
  if (operator == '+') {
    addTwoBinaryNumbers(firstBinary, secondBinary, result);

  // if operator is -, convert the second binary number using two's twosComplement
  // and add the two binary numbers
  } else if (operator == '-') {
    twosComplement(secondBinary);
    addTwoBinaryNumbers(firstBinary, secondBinary, result);
  }

  // output the result of the operation
  for (int i = 0; i <= 7; i++) {
    printf("%d", result[i]);
  }

  printf("\n");
}

void decimalToBinary(int decimalNumber, int* binaryNumber) {
  int remainder;

  // iterates for each element in the binaryNumber array
  // from the LSB to the MSB
  for (int i = 7; i >= 0; i--) {

    // calculate the absolute remainder when the decimal number is divided by 2, i.e. 0 or 1
    remainder = abs(decimalNumber % 2);

    // add the remainder to the correct position in the binaryNumber array
    binaryNumber[i] = remainder;

    // integer divison by 2 performed on the decimal number
    decimalNumber = decimalNumber / 2;
  }
}

void twosComplement(int* binaryNumber) {
  // while the LSB is not 1, move to the next MSB
  int i = 7;
  while (binaryNumber[i] != 1) {
    i--;
  }

  // the current bit is 1, so move to the next MSB
  i--;

  // flip the current bit and move to the next MSB
  for (int j = i; j >= 0; j--) {
    binaryNumber[j] = binaryNumber[j] == 0 ? 1 : 0;
  }
}

void addTwoBinaryNumbers(int* binaryNumberOne, int* binaryNumberTwo, int* finalBinaryNumber) {
  // carry bit initialised with a value of 0
  int carry = 0;

  // starting with the LSB, move to the next MSB
  for (int i = 7; i >= 0; i--) {

    // adds the values of the bits at position n in the binary numbers
    switch (binaryNumberOne[i] + binaryNumberTwo[i]) {

      // if the sum is 0, set the value of the bit in the resulting binary number
      // to that of the carry, and reset carry to 0
      case 0:
        finalBinaryNumber[i] = carry;
        carry = 0;
        break;

      // if the sum is 1 and the carry is 0, set the value of the bit in the
      // resulting binary number to 1, otherwise the carry is 1, so set the
      // value of the bit in the resulting binary number to 0 and set the carry to 1
      case 1:
        if (carry == 0) {
          finalBinaryNumber[i] = 1;
        } else {
          finalBinaryNumber[i] = 0;
          carry = 1;
        }
        break;

      // if the sum is 2 and the carry is 0, set the value of the bit in the
      // resulting binary number to 0, otherwise the carry is 1, so set the
      // value of the bit in the resulting binary number to 1
      // carry bit will always be 1
      case 2:
        finalBinaryNumber[i] = carry == 0 ? 0 : 1;
        carry = 1;
        break;
    }
  }
}

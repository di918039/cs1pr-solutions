#include <stdio.h>

/* This program shall output the concatenation of all arguments together */
int main(int argc, char * argv[]){

  for(int i=0; i < argc; i++){
    printf("%c", argv[i]);
  }

  printf("\n");

  // Now we end the program
  return 0;
}

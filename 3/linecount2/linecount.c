#include <stdio.h>

void main(int argc, char *argv[]) {
  //Two variables to store the line counter and the current character
  int lineCount = 0;
  char currentCharacter = getchar();

  //While loop to retrieve characters using getchar() and count the lines
  while (currentCharacter != EOF) {

    //If statement to check if the current character is a new lines
    if (currentCharacter == '\n') {
      lineCount++;
    }

    currentCharacter = getchar();
  }

  //Print the line counter to stdout
  printf("Line Counter: %d\n", lineCount);
}

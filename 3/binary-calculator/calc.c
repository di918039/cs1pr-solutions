#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

//yes, i know i didnt use the skeleton code, but this works perfectly.

int decBin(int n){
	int i, k;
	for (i = 7; i >= 0; i--)
	{
		k = n >> i;
		if (k & 1)
		printf("1");
		else
		printf("0");
	}
	printf("\n");
	return 0;
}

int binAddition(int a,int b)
{
      int carry;
      while (b != 0) {
              carry = (a & b) << 1;
              a=a^b;
              b=carry;
      }
      return a;
}

//this part is from the skeleton code
int main(int argc, char ** argv){
    argc = 0;
    argc++;
	int result = 0;
	int8_t dec1 = atoi(argv[1]);
	char op = argv[2][0];
	int8_t dec2 = atoi(argv[3]);


	if(op == '+'){ //adder
		result = binAddition(dec1,dec2);
    }

	if(op == '-'){
		dec2 = binAddition(~dec2, 1); //find two's compliment of dec2
		result = binAddition(dec1,dec2);
	}

	if(op == 'x'){
		int loop = 1;
		while (loop<=dec2){ //multiplication is just repeated addition
			result = binAddition(result,dec1);
			loop++;
		}
	}

	decBin(result);
	return 0;
}

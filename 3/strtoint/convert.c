#include <stdio.h>

int main(int argc, char **argv){
  if(argc != 1){
    // check that no argument is given on the cmd line
    printf("Synopsis: %s\n", argv[0]);
    return 1;
  }

  int c;
  int number = 0;
  while((c = getchar()) != EOF){
    if(c >= '0' && c <= '9'){
      number = number * 10 + (c - '0');
    }else if(c != '\n'){
      printf("Invalid character: %c\n", c);
    }
  }
  printf("%d\n", number*2);

  return 0;
}

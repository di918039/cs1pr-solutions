#include <stdio.h>
#include <string.h>
#include <math.h>
#include "my_lib.h"

//input argv[1] = "mean"
//		argv[2->] = integer

int main(int argc, char ** argv){
	int i = 1;
	int numbers[(argc-2)];
	int op;
	char * operation = argv[1];
	float (*fun_ptr)(int, int);
	float average = 0;

	for(i=2; i<argc; i++){
		numbers[(i-1)] = my_atoi(argv[i]);
	}
	
	if (strcmp(operation, "mean") == 0)
		op = 1;
	if (strcmp(operation, "max") == 0)
		op = 2;
	if (strcmp(operation, "sd") == 0)
		op = 3;

	switch (op){
		case(1):{
			fun_ptr = &mean;
			break;
		}
		case(2):{
			fun_ptr = &max;
			break;
		}
		case(3):{
			fun_ptr = &standardDev;
			break;
		}
		default:
			printf("invalid operation\n");
			break;
	}
	float ans = (*fun_ptr)(numbers, (argc-2));
	printf("%.1f\n",ans);
	return 0;
}
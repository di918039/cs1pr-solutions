#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "my_lib.h"

//input argv[1] = "mean"
//		argv[2->] = integer

int main(int argc, char ** argv){
	int i = 1;
	int numbers[(argc-2)];
	int switcher = 0;
	char * operation = argv[1];
	float average = 0;

	for(i=2; i<argc; i++){
		numbers[(i-1)] = my_atoi(argv[i]);
	}

	if (strcmp(operation, "mean") == 0)
		switcher = 1;
	if (strcmp(operation, "max") == 0)
		switcher = 2;
	if (strcmp(operation, "sd") == 0)
		switcher = 3;

	float ans = 0.0;
	switch (switcher){
		case 1:{
			ans = (mean(numbers,(argc-2)));
			break;
		}
		case 2:{
			ans = (max(numbers,(argc-2)));
			break;
		}
		case 3:{
			ans = (standardDev(numbers, (argc-2)));
			ans = sqrt(ans);
			break;
		}
		default:
			printf("invalid operation\n");
			exit(1);
			break;
	}

	printf("%.1f\n",ans);
	return 0;
}

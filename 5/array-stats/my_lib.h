#ifndef MY_LIBRARY_H
#define MY_LIBRARY_H
/**
*defines the function usage
*/
int my_atoi(char * str);
int mean(int numbers[], int size);
int max(int numbers[], int size);
int standardDev(int numbers[], int size);
#endif

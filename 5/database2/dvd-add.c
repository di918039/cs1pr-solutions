#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

/**
 * Program that will input the entry for a DVD into a database.
 *
 * @param argc Number of arguments passed to the program.
 * @param argv Character array of command line arguments.
 *
 * @return Exit status code of the program.
 */
int main(int argc, char ** argv) {

  //  If the length of the title is too long, raise an error
  if (strlen(argv[1]) > 32) {
    printf("Error: Title of DVD too long.\n");
    return 1;
  }

  //  If the length of the DVD is not between 0 and 150, raise an error
  if (atoi(argv[2]) < 0 || atoi(argv[2]) > 150) {
    printf("Error: Invalid DVD duration.\n");
    return 1;
  }

  // Initialise database file name and file pointer
  char * fileName = "dvd.dat";
  FILE * filePointer = NULL;
  int dvdID;

  //  Take Title and Length arguments
  char * title = argv[1];
  int length = atoi(argv[2]);

  //  If 'dvd.dat' file doesn't exist
  if (access(fileName, F_OK) == -1) {

    //  dvdID variable equals 0
    dvdID = 0;

    //  Create file
    filePointer = fopen(fileName, "a");

    //  Add ID, Title and Length; {ID,Name,TotalLength}
    fprintf(filePointer, "ID,Name,TotalLength\n");

    //  Add information for DVD; {2348,TheCProgrammingLanguage,192}
    fprintf(filePointer, "%d,%s,%d\n", dvdID, title, length);

  // Otherwise 'dvd.dat' exists
  } else {

    //  Initialise the character array used for holding the last line and
    //  assign the dvdID an impossible value
    char lastLine[64] = { 0 };
    dvdID = -1;

    //  Open file
    filePointer = fopen(fileName, "r+");

    //  Go through each line until the end of the file is reached
    while (fgets(lastLine, sizeof(lastLine), filePointer) != NULL) {

      //  If the ID of the current DVD is greater than the currently stored ID
      //  update ID to the new ID
      if (atoi(strtok(lastLine, ",")) > dvdID) {
        dvdID = atoi(strtok(lastLine, ","));
      }
    }

    //  Increase ID by 1 so that it is unique - there should be no ID greater
    //  or equal to this ID
    dvdID++;

    //  Add information for DVD; {2348,TheCProgrammingLanguage,192}
    fprintf(filePointer, "%d,%s,%d\n", dvdID, title, length);

  }

  //  Close the file
  fclose(filePointer);

  //  Output 'Added ID {ID number}'
  printf("Added ID %d\n", dvdID);

  return 0;
}

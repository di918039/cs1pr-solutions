#ifndef MY_LIB_H
#define MY_LIB_H

/**
 * Calculates the mean of an integer array.
 *
 * @param numbers The array from which the mean is calculated.
 * @param size The number of elements in the array.
 *
 * @return The mean of the integer array.
 */
float i_statsMean(int *, int);

/**
 * Calculates the mean of a float array.
 *
 * @param numbers The array from which the mean is calculated.
 * @param size The number of elements in the array.
 *
 * @return The mean of the float array.
 */
float f_statsMean(float *, int);

/**
 * Calculates the highest number from an integer array.
 *
 * @param numbers The array from which the highest number is determined.
 * @param size The number of elements in the array.
 *
 * @return The highest number from the integer array.
 */
float statsMax(int *, int);

/**
 * Calculates the square root of a number.
 *
 * @param number The number for which the square root is calculated.
 *
 * @return The square root of the number.
 */
float squareRoot(float);

/**
 * Calculates the standard deviation of an integer array.
 *
 * @param numbers The array from which the standard deviation is calculated.
 * @param size The number of elements in the array.
 *
 * @return The standard deviation of the integer array.
 */
float statsSD(int *, int);

#endif

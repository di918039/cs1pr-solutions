#include <stdio.h>

float i_statsMean(int * numbers, int size) {
  int sum = 0;

  for (int i = 0; i < size; i++) {
    sum = sum + numbers[i];
  }

  return (float)sum / (float)size;
}

float f_statsMean(float * numbers, int size) {
  float sum = 0;

  for (int i = 0; i < size; i++) {
    sum = sum + numbers[i];
  }

  return sum / (float)size;
}

float statsMax(int * numbers, int size) {
  int highestNumber = numbers[0];

  for (int i = 1; i < size; i++) {
    if (numbers[i] > highestNumber) {
      highestNumber = numbers[i];
    }
  }

  return highestNumber;
}

float squareRoot(float number) {
  float root = number / 3;

  if (number <= 0) {
    return 0;
  }

  for (int i = 0; i < 32; i++) {
    root = (root + number / root) / 2;
  }

  return root;
}

float statsSD(int * numbers, int size) {
  float meanNumbers = i_statsMean(numbers, size);
  float temp;
  float squaredDifferences[size];
  float meanSquaredDifferences;

  for (int i = 0; i < size; i++) {
    temp = numbers[i] - meanNumbers;
    temp = temp * temp;
    squaredDifferences[i] = temp;
  }

  meanSquaredDifferences = f_statsMean(squaredDifferences, size);

  return squareRoot(meanSquaredDifferences);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "my_lib.h"

int main(int argc, char **argv) {

  // Instantiation the charater array holding the operation
  char * operation = argv[1];

  // Declare the integer array that will hold the numbers thar are passed as
  // command line arguments
  int numbers[argc - 1];

  // Convert each argument into an integer and add to the integer array
  for (int i = 2; i < argc; i++) {
    numbers[i - 2] = atoi(argv[i]);
  }

  // Calculate the number of elements in the integer array
  int size = (sizeof(numbers) / sizeof(numbers[0])) - 1;

  // Declare the variable to hold the result
  float result;

  // Call the correct operator function
  if (strcmp(operation, "mean") == 0) {
    result = i_statsMean(numbers, size);
  } else if (strcmp(operation, "max") == 0) {
    result = statsMax(numbers, size);
  } else if (strcmp(operation, "sd") == 0) {
    result = statsSD(numbers, size);
  }

  // Print the result
  printf("%.1f\n", result);

  return 0;
}

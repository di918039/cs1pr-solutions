#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>
#include <math.h>

bool isNumber(char *);
int printIEEE754(long double, bool);
int printSignBit(long double, bool);
int printExponentBits(long double);
int printMantissaBits(long double);

int main(int argc, char **argv) {

  bool minusZero = false;

  if (strcmp(argv[1], "NaN") == 0) {

    // Print the IEEE 754 representation of NaN
    printf("0|11111111|11111111111111111111111\n");

  } else if (strcmp(argv[1], "Infinity") == 0) {

    // Print the IEEE 754 representation of Infinity
    printf("0|11111111|00000000000000000000000\n");

  } else if (isNumber(argv[1])) {

    // Print the IEEE 754 representation of the number
    long double number = strtod(argv[1], NULL);

    if (argv[1][0] == '-' && number == 0) {
      minusZero = true;
    }

    printIEEE754(number, minusZero);

  } else {

    // Invalid argument passed; exit
    exit(1);

  }

  return 0;
}

/**
 * Checks if a string is a number.
 *
 * @param string The character array that is checked.
 *
 * @return Boolean that states whether the given string is a number or not.
 */
bool isNumber(char * string) {
  int i = 0;
  bool decimalNumber = false;

  // If the first character is a minus sign or plus sign, move on to the next character
  if (string[i] == '-' || string[i] == '+') {
    i++;
  }

  // For each character in the character array
  for ( ; string[i] != 0; i++) {

    // If the current character is a decimal point and a decimal point hasn't
    // appeared before, set decimalNumber to true and move on to the next character
    if (string[i] == '.' && decimalNumber == false) {
      decimalNumber = true;
      i++;
    }

    // If the current character isn't a number, return false
    if (!isdigit(string[i])) {
      return false;
    }

  }

  return true;
}

/**
 * Converts an integer into its binary equivalent.
 *
 * @param decimalNumber The integer that is to be converted.
 * @param length The number of bits for which the binary number is calculated.
 * @param binaryNumber The integer array containing the binary representation of the original integer.
 *
 * @return Exit status code of the function.
 */
int decimalToBinary(int decimalNumber, int length, int* binaryNumber) {
  int remainder = 0;

  // Get the absolute value of the integer decimalNumber
  if (decimalNumber < 0) {
    decimalNumber = decimalNumber - 2 * decimalNumber;
  }

  // Iterates for each element in the binaryNumber array from the LSB to the MSB
  for (int i = length - 1; i >= 0; i--) {

    // Calculate the remainder when the decimal number is divided by 2, i.e. 0 or 1
    remainder = decimalNumber % 2;

    // Insert the remainder into the correct position in the binaryNumber array
    binaryNumber[i] = remainder;

    // Integer division by 2 peformed on the decimal number
    decimalNumber = decimalNumber / 2;
  }

  return 0;
}

/**
 * Prints the number in the IEEE 754 floating point representation.
 *
 * @param number The decimal number that is to be represented using IEEE 754.
 * @param minusZero The boolean stating if the number is negative zero or not.
 *
 * @return Exit status code of the function.
 */
int printIEEE754(long double number, bool minusZero) {

  printSignBit(number, minusZero);
  printf("|");
  printExponentBits(number);
  printf("|");
  printMantissaBits(number);
  printf("\n");

  return 0;
}

/**
 * Prints the sign bit of a given number.
 *
 * @param number The number for which the sign bit is determined.
 * @param minusZero The boolean stating if the number is negative zero or not.
 *
 * @return Exit status code of the function.
 */
int printSignBit(long double number, bool minusZero) {

  if (minusZero) {
    printf("1");
    return 0;
  }

  if (number < 0) {
    printf("1");
  } else {
    printf("0");
  }

  return 0;
}

/**
 * Prints the exponent bits of a given number.
 *
 * @param number The number for which the exponent bits are determined.
 *
 * @return Exit status code of the function.
 */
int printExponentBits(long double number) {
  int exponent = 0;
  int length = 8;
  int exponentBinary[8] = { 0 };

  // Get the absolute value of the float number
  if (number < 0) {
    number = number - 2 * number;
  }

  // If the number is less than 0, but not 0, multiply by 2 until the number is
  // between 1 and 2
  if (number < 1 && number != 0) {

    while (number < 1) {
      number = number * 2;
      exponent--;
    }

  // Otherwise number is greater than or equal to 1, so divide by 2 until the
  // number is between 1 and 2
  } else {

    while (number >= 2) {
      number = number / 2;
      exponent++;
    }

  }

  // If the number is not 0
  if (number != 0) {

    // Convert the decimal number to its binary equivalent
    decimalToBinary(127 + exponent, length, exponentBinary);
  }

  // Print each bit of the binary number in order from the MSB to the LSB
  for (int i = 0; i <= length - 1; i++) {
    printf("%d", exponentBinary[i]);
  }

  return 0;
}

/**
 * Prints the mantissa bits of a given number.
 *
 * @param number The number for which the mantissa bits are determined.
 *
 * @return Exit status code of the function.
 */
int printMantissaBits(long double number) {
  int length = 23;
  int mantissaBinary[23] = { 0 };

  // Get the absolute value of the float number
  if (number < 0) {
    number = number - 2 * number;
  }

  // If the number is less than 0, but not 0, multiply by 2 until the number is
  // between 1 and 2
  if (number < 1 && number != 0) {

    while (number < 1) {
      number = number * 2;
    }

  // Otherwise number is greater than or equal to 1, so divide by 2 until the
  // number is between 1 and 2
  } else {

    while (number >= 2) {
      number = number / 2;
    }

  }

  // Remove the '1' preceding the radix point
  number--;

  // Insert the correct bit in the correct position, from the MSB to the LSB
  for (int i = 0; i <= length - 1; i++) {

    number = number * 2;

    if (number >= 1) {
      mantissaBinary[i] = 1;
      number--;
    } else {
      mantissaBinary[i] = 0;
    }

  }

  // Print each bit of the binary number in order from the MSB to the LSB
  for (int i = 0; i <= length - 1; i++) {
    printf("%d", mantissaBinary[i]);
  }

  return 0;
}

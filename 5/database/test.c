#include <stdio.h>


// move this to DVD.h AND change the structure to match what a DVD should contain
struct data_t{
	int id;
	char * name;
	int duration;
};
typedef struct data_t data_t;

int main(int argc, char ** argv){
	int ret;

	// open our database for reading and writing without removing any existing data! Mode: r+
	FILE * fd = fopen("data.dat", "r+");
	// move to the correct position, here position 8
	ret = fseek(fd, 8, SEEK_CUR);

	// write the data, which is here a single record containing the information in the struct:
	data_t data = {.a = 'a', .b = 4711};
	ret = fwrite(& data, sizeof(data_t), 1, fd);
	fclose(fd);

	return 0;
}
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dvd.h"

int main(int argc, char * argv[]){
    FILE * outfile;
	
	outfile = fopen("data.dat", "r+");
    if (outfile == NULL) 
    { 
		printf("file does not exist or is empty.\n");
		exit(1);
    }
	int requested = 0;
	struct data_t filedata;
	printf("ID,Name,TotalLength\n");
	if(argc == 2){
		requested = atoi(argv[1]);
	}
	
	while(fread(&filedata, sizeof(data_t), 1, outfile)){
		
		if(requested == 0){
			//print ID
			printf("%d,",filedata.id);
			//print title
			printf("%s,",filedata.title);			
			//print duration
			printf("%d\n",filedata.duration);			
		}
		
		if(filedata.id == requested){
			//print ID
			printf("%d,",filedata.id);		
			//print title
			printf("%s,",filedata.title);
			//print duration
			printf("%d\n",filedata.duration);
		}		
	}

    fclose(outfile);

    return 0;
}
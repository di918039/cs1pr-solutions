#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dvd.h"

int main(int argc , char * argv[]){
    FILE * outfile;
	FILE * tmpFile;
	
	//open data.dat and a temporary file
	outfile = fopen("data.dat", "r+");
    if (outfile == NULL) 
    { 
		printf("Error : The file does not exist.");
		exit(1);
    }	
	tmpFile = fopen("temp.dat", "w+");
	
	
	//define the id value to be removed and 2 structure variables
	int removal = atoi(argv[1]);
	struct data_t filedata;
	struct data_t hold;
	
	//save all contents of data.dat to a temp file except for deleted record(s)
	while(fread(&filedata, sizeof(data_t), 1, outfile)){
		if(filedata.id != removal){
			hold.id = filedata.id;
			strcpy(hold.title, filedata.title);
			hold.duration = filedata.duration;
			/* Write data to file */
			fwrite(&hold, sizeof(data_t), 1, tmpFile);			
		}		
	}
	
	//delete the data.dat file
	fclose(outfile);
	remove("data.dat");
	
	//rename temp file to data.dat
	rename("temp.dat", "data.dat");
	
	//close file
	printf("Deleted %d\n",removal);
	fclose(tmpFile);
    return 0;
}
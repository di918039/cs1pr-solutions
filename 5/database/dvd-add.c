#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dvd.h"

int main(int argc , char * argv[]){
    FILE * outfile;

	outfile = fopen("data.dat", "r+");
    if (outfile == NULL)
    {
		outfile = fopen("data.dat","w+");
    }

	int time = atoi(argv[2]);
	char * name = argv[1];
	struct data_t filedata;

	//generates a unique ID
	int count = 0;
	while(fread(&filedata, sizeof(data_t), 1, outfile)){
		count = filedata.id;
	}
	int uniqueID = (1 + count);


	//struct data_t dvd = {.id = uniqueID, .title = name, .duration = time};
	struct data_t dvd;


	dvd.id = uniqueID;
	dvd.duration = time;
	strcpy(dvd.title, name);


    /* Write data to file */
    fwrite(&dvd, sizeof(data_t), 1, outfile);
    if(fwrite != 0)
        printf("Added ID %d\n",dvd.id);
    else
        printf("error writing file !\n");

    /* Close file to save file data */
    fclose(outfile);

    return 0;
}

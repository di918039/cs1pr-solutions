#include <stdio.h>
#include "qsort.h"

void swap(int * array, int a, int b){
	int hold = array[a];
	array[a] = array[b];
	array[b] = hold;
}

void mqsort(int * number,int lo,int hi){
   int i, j, pivot, temp;

   if(lo<hi){
      pivot=lo;
      i=lo;
      j=hi;

      while(i<j){
         while(number[i]<=number[pivot]&&i<hi)
            i++;
         while(number[j]>number[pivot])
            j--;
         if(i<j){
			//swap elements
			swap(number, i, j);
         }
      }

	  //swap elements
	  swap(number, pivot, j);
	  
      mqsort(number,lo,j-1);
      mqsort(number,j+1,hi);

   }
}
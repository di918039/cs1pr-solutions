#include <stdio.h>
#include <stdlib.h>
#include "qsort.h"

int main(int argc, char * argv[]){
	//if no user input
	if(argc == 1){
		exit(1);
	}
	
	int numbers[(argc-1)];
	for(int i=1; i<argc; i++){
		numbers[(i-1)] = atoi(argv[i]);
	}
	
	mqsort(numbers, 0, argc-2);
	
	for(int i = 0; i < (argc-2); i++){
		printf("%d ",numbers[i]);
	}
	printf("%d\n",numbers[argc-2]);
}
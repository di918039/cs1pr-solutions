#include "table.h"
#include <stdio.h>

//create the 2D array in the table.
void tbl_init(lookupTbl_t * tbl){
	for(int x = 0; x < 10; x++){
		for(int y = 0; y < 10; y++){
			tbl->table[x][y] = 0;
		}
	}
}

//change the value of a cell of the table
void tbl_set(lookupTbl_t * tbl, int x, int y, void * ptr){
  tbl->table[x][y] = ptr;
}

//return a value fromt the table as a pointer
void* tbl_get(lookupTbl_t * tbl, int x, int y){
	return(tbl->table[x][y]);
}


void print_matrix(lookupTbl_t * tbl){
	for(int x = 0; x < 10; x++){
		for(int y = 0; y < 10; y++){
//			printf("\t%p",tbl_get(tbl, x, y));
			printPtr(tbl_get(tbl, x, y));
		}
		printf("\n");
	}
}

void printPtr(void * ptr){
  char buff[15];
  // print the pointer to the buffer to see how long it will be
  int len = sprintf(buff, "%p", ptr);
  // add whitespace up to 15 chars
  for(int i = 15 - len; i >=0; i--){
    printf(" ");
  }
  printf("%s", buff);
}
#include "table.h"
#include <stdio.h>

// Testing
int main(){
  lookupTbl_t table;
  tbl_init(& table);

  // store ptr on location (x, y), here we store the number instead of a pointer for testing
  void * ptr = (void*) 4711;
  tbl_set(&table, 5, 3, ptr);

  // should return the value on position (x, y); return NULL if not set
  void * val = tbl_get(&table, 5, 3);
  // val must be identical to the ptr!
  if(val != ptr){
    printf("Error, the lookup table doesn't work!\n");
  }
  

  tbl_set(&table, 0, 0, (void*) 9); // set another fake pointer

  // print the output on stdout use the %p format specifier with printf()
  print_matrix(&table);

}

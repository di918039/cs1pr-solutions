#ifndef TABLE_H
#define TABLE_H

/**
 * Create a structure of type lookupTbl_t.
 */
typedef struct lookupTbl_t {
  void * table[10][10];
} lookupTbl_t;

/**
 * Initialise an object of type table.
 *
 * @param The pointer to the table.
 */
void tbl_init(lookupTbl_t * tbl);

/**
 * Set the value of any element in the table to a pointer.
 *
 * @param The pointer to the table.
 * @param The x co-ordinate of the element.
 * @param The y co-ordinate of the element.
 * @param The pointer that is to be set in the table.
 */
void tbl_set(lookupTbl_t * tbl, int x, int y, void * ptr);

/**
 * Get a pointer from the table.
 *
 * @param The pointer to the table.
 * @param The x co-ordinate of the element.
 * @param The y co-ordinate of the element.
 *
 * @return The pointer that is to be retrieved.
 */
void * tbl_get(lookupTbl_t * tbl, int x, int y);

/**
 * Print a pointer with sufficient padding.
 *
 * @param The pointer that is to be printed.
 */
void printPointer(void * pointer);

/**
 * Print the table.
 *
 * @param The pointer to the table.
 */
void print_matrix(lookupTbl_t * tbl);

#endif

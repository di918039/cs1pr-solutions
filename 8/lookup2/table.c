#include <stdio.h>
#include "table.h"

void tbl_init(lookupTbl_t * tbl) {

  for (int i = 0; i < 10; i++) {
    for (int j = 0; j < 10; j++) {
      tbl->table[j][i] = 0;
    }
  }

}

void tbl_set(lookupTbl_t * tbl, int x, int y, void * ptr) {
  tbl->table[y][x] = ptr;
}

void * tbl_get(lookupTbl_t * tbl, int x, int y) {

  if (tbl->table[y][x] != NULL) {
    return tbl->table[y][x];
  } else {
    return NULL;
  }

}

void printPointer(void * pointer) {

  char buffer[15];
  int len = sprintf(buffer, "%d", pointer);

  for (int i = 15 - len; i >= 0; i--) {
    printf(" ");
  }

  printf("%s", buffer);

}

void print_matrix(lookupTbl_t * tbl) {

  for (int i = 0; i < 10; i++) {
    for (int j = 0; j < 10; j++) {
      printPointer(tbl->table[j][i]);
    }
    printf("\n");
  }

}

#include <stdio.h>
#include "table.h"

/**
 * Main entrypoint to the program.
 *
 * @param Number of arguments passed to the program.
 * @param Character array of command line arguments.
 *
 * @return Exit status code of the program.
 */
int main(int argc, char ** argv) {

  // Declare a variable of type lookupTbl_t
  lookupTbl_t table;

  // Initialise the table
  tbl_init(&table);

  // Store a pointer in any location in the table
  void * pointer1 = (void *) 4711;
  tbl_set(&table, 5, 3, pointer1);

  // Get a pointer from the table
  void * pointer2 = tbl_get(&table, 5, 3);
  void * pointer3 = tbl_get(&table, 2, 3);
  printf("pointer 2: %p\npointer 3: %p\n", pointer2, pointer3);

  // Print the entire table
  print_matrix(&table);

  return 0;
}

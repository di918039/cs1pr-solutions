#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "qsort.h"

/**
 * Main entrypoint to the program.
 * Sorts a list of numbers using the quick sort algorithm.
 *
 * @param Number of arguments passed to the program.
 * @param Character array of command line arguments.
 *
 * @return Exit status code of the program.
 */
int main(int argc, char ** argv) {

  // If any numbers haven't been passed, return general error code
  if (argc < 2) {
    return 1;
  }

  // Calculate the number of numbers
  int size = argc - 1;

  // Declare integer array that will hold the numbers passed as command
  // line arguments
  int numbers[size];

  // Convert command line arguments to integers and store them in the numbers array
  for (int i = 1; i < argc; i++) {
    numbers[i - 1] = atoi(argv[i]);
  }

  // Call the quick sort function
  mqsort(numbers, size);

  // Print out the sorted numbers list
  for (int i = 0; i < size; i++) {
    printf("%d ", numbers[i]);
  }
  printf("\n");

  return 0;
}

#include <list.h>
#include <stdio.h>

static void traversefunc(void * data){
  printf("%p\n", data);
}

int main(){
  list_t * l = list_init((void*) 55);
  list_append(l, (void*) 60);

  list_iterate_fwd(l, traversefunc);
  list_free(l, NULL);

  return 0;
}

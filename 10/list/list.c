#include <stdlib.h>
#include <assert.h>

#include <list.h>

// hidden implementation
struct list_t{
  void * value;
  list_t * next;
  list_t * prev;
};

list_t * list_init(void * value){
  list_t* l = malloc(sizeof(list_t));
  l->value = value;
  l->next = NULL;
  l->prev = NULL;
  return l;
}

list_t * list_append(list_t * o, void * value){
  list_t* n = malloc(sizeof(list_t));
  n->next = o->next;
  n->value = value;
  n->prev = o;
  // link into existing structure
  o->next = n;
  return n;
}

void list_iterate_fwd(list_t * lst, void (*traversefunc)(void* data)){
  while(lst != NULL){
    traversefunc(lst->value);
    lst = lst->next;
  }
}

void list_free(list_t * lst, void (*freefunc)(void* data)){
  if(lst->prev != NULL){
    lst->prev->next = NULL;
  }

  while(lst != NULL){
    if(freefunc){
      freefunc(lst->value);
    }
    list_t * next = lst->next;
    // the order is important!
    free(lst);
    lst = next;
  }
}

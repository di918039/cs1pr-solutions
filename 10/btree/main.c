#include <stdio.h>
#include "tree.h"

void printtree(int id, double value){
  printf("%d=%f\n", id, value);
}

int main(){
  btree_t * t = btree_init();

  for(int i=0; i < 6; i++){
    int id = btree_insert(t, i);
    printf("%d\n", id);
  }

  btree_remove(t, 5);

  btree_traverse_preorder(t, printtree);

  btree_free(t);
  return 0;
}

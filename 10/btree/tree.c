#include <stdlib.h>
#include <assert.h>

#include "tree.h"

struct btree_t{
  int size;
  int capacity;
  double * data;
};

btree_t * btree_init(){
  btree_t* t = malloc(sizeof(btree_t));
  t->size = 0;
  t->capacity = 10;
  t->data = malloc(t->capacity * sizeof(double));
  return t;
}

int btree_insert(btree_t * t, double value){
  if(t->size == t->capacity){
    // must expand the tree
    t->capacity *= 2;
    // grow the memory or create new one and copy data over
    t->data = realloc(t->data, t->capacity * sizeof(double));
  }
  t->data[t->size] = value;
  return t->size++;
}

double btree_remove(btree_t * t, int id){
  assert(id < t->size);
  t->size--;
  double val = t->data[id];
  t->data[id] = t->data[t->size];
  return val;
}

static void internal_tree_iter(btree_t * t, int id, void (*traversefunc)(int id, double value)){
  // 0, 1, 2, 3, 4, 5, 6, 7, 8
  // go left
  int lid = id*2 + 1;
  int rid = (id + 1) * 2;
  if(lid <= t->size){
    internal_tree_iter(t, lid, traversefunc);
  }
  // print item
  traversefunc(id, t->data[id]);
  // go right
  if(rid <= t->size){
    internal_tree_iter(t, rid, traversefunc);
  }
}

void btree_traverse_preorder(btree_t * tree, void (*traversefunc)(int id, double value)){
  internal_tree_iter(tree, 0, traversefunc);
}

void btree_free(btree_t * t){
  free(t->data);
  free(t);
}

bool btree_is_empty(btree_t * t){
  return t->size == 0;
}

int btree_size(btree_t * t){
  return t->size;
}

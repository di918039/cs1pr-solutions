#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

//a function to print an error message and exit the program
int error(){
	printf("Error: Invalid input\n");
	exit(1);
}

int Encode(char * string){
	int count = 0;
	
	//validate format of input (must be all characters or whitespace)
	for (int i = 0; string[i] != '\0'; i++){
		if (isalpha(string[i]) == 0 && string[i] != ' '){
			error();
			return 0;
		}
	}
	
	//reads through every character in string
	for (int i = 1; string[i] != '\0'; i++){
		//increments count by 1 if two adjacent characters are the same
		if(string[i] == string[i-1] && count < 9){
			count++;
		}else{
			//prints character with the count value
			printf("%c%d",string[i-1],count);
			count = 0;
		}
	}
	//prints final character and \n
	printf("%c%d\n",string[strlen(string)-1],count);
	return 0;
}


int Decode(char * string){
	int repeat;
	
	//validates the input string format (character or whitespace followed by a digit)
	for (int i = 0; string[i] != '\0'; i += 2){
		if(isalpha(string[i]) == 0 && string[i] != ' ' || isdigit(string[i+1]) == 0){
			error();			
		}					
	}
	
	
	//repeats for every character in string
	for (int i = 0; string[i] != '\0'; i += 2){
		//converts character digit to an integer
		repeat = string[i+1] - '0';
		//prints letter specified number of times
		for(int j = 0; j <= repeat; j++){
			printf("%c",string[i]);
		}
	}
	printf("\n");
	return 0;
}

int main(int argc,char * argv[]){
	char * operation = argv[1];
	char * inputChar = argv[2];
	//checks which operation the user has requested and runs the correct function
	if (strcmp(operation, "D") == 0){
		Decode(inputChar);
	}else if (strcmp(operation, "E") == 0){
		Encode(inputChar);
	}else{
		error();
	}
	return 0;
}
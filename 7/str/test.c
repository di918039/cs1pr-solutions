#include <cstrlib.h>
#include <stdio.h>
#include <assert.h>

int main(){
  str_t t1, t2;
  str_init("Hello world", & t1);
  str_init("Hello sam", & t2);

  printf("%d %d, equal: %d\n", str_len(& t1), str_len(& t2), str_equal(& t1, & t2));
  str_cpy(& t1, & t2);
  printf("%d %d, equal: %d\n", str_len(& t1), str_len(& t2), str_equal(& t1, & t2));

  return 0;
}

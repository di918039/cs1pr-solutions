#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include <cstrlib.h>

static int computeHash(char const * str){
  int hash = 0;
  while(*str != 0){
    hash = hash * 3711 + *str;
    str++;
  }
  return 0;
}

void str_init(char const * str, str_t * out){
  assert(out != NULL);
  assert(str != NULL);
  out->str = str;
  out->len = strlen(str);
  out->hash = computeHash(str);
}

void str_cpy(str_t * in, str_t * out){
  out->str = in->str;
  out->len = in->len;
  out->hash = in->hash;
}

int str_len(str_t * str){
  return str->len;
}

int str_hash(str_t * str){
  return str->hash;
}

bool str_equal(str_t * a, str_t * b){
  return str_cmp(a, b) == 0;
}

int str_cmp(str_t * a, str_t * b){
  if(a->len != b->len){
    return a->len - b->len;
  }
  if(a->hash != b->hash){
    return a->hash - b->hash;
  }

  return strcmp(a->str, b->str);
}

#include <strlib.h>
#include <stdio.h>

int main(int argc, char** argv){
  str_t str1;
  str_init("Hello world", & str1);

  str_t str2;
  //str_cpy(& str1, & str2);
  str_init("world Hello", & str2);
  printf("%d : \"%s\" hash: %d\n", str2.len, str2.str, str2.hash);

  return 0;
}

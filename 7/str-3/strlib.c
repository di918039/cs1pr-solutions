#include <strlib.h>
#include <string.h>
#include <assert.h>

void str_init(char const * str, str_t * out){
  assert(str);
  assert(out);
  // copy over the string into out->str
  // test\0
  // out->str = 4712
  char * sout = out->str;
  int hash = 0;
  for( ; *str != 0 ; str++, sout++){
    *sout = *str;
    hash = (hash<<2) + *str;
  }
  *sout = '\0';

  out->len = sout - out->str;
  out->hash = hash;
}

void str_cpy(str_t * in, str_t * out){
  // hack / slow: str_init(in->str, out);
  char * sout = out->str;
  char * sin = in->str;
  for( ; *sin != 0 ; sin++, sout++){
    *sout = *sin;
  }
  out->len = in->len;
  out->hash = in->hash;
}

int str_len(str_t * str){
  return str->len;
}

int str_hash(str_t * str){
  return str->hash;
}

bool str_equal(str_t * a, str_t * b){
  return str_cmp(a, b) == 0;
}

int str_cmp(str_t * a, str_t * b){
  if( a->len < b->len ){
    return -1; // length lexicographical ordering
  }
  if( b->len > a->len ){
    return +1; // length lexicographical ordering
  }

  if( a->hash == b->hash ){
    // likely the same ^-^
    return strcmp(a->str, b->str);
  }

  return a->hash - b->hash;
}

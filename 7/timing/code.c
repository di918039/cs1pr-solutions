#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int for_loop(int loops){
  int total = 0;
  for(int i = 1; i<= loops; i++){
    total += i;
  }
  return 0;
}


int str_cmp(char * str, char * str2){
  if (strcmp(str, str2) == 0){
    return 1;
  }
  return 0;
}


int recursive(int total, int n){
  if (n > 0){
    total += n;
    recursive(total, n-1);
  }
  return 0;
}


int main(int argc, char * argv[]){
  printf("n, for loop, recursive, strcmp\n");
  for(int i=1; i<argc; i++){
    clock_t t_start;
    clock_t t_end;
    int n = atoi(argv[i]);

//create strings of a specified length
    char string[n];
	  char string2[n];
    for(int i = 1; i <= n; i++){
      string[i] = '*';
    }
	strcpy(string2,string);

//timing for loop
    t_start = clock();
    for_loop(n);
    t_end = clock();
    double tFor = ((double) (t_end - t_start)) / CLOCKS_PER_SEC;

//timing recursive
    t_start = clock();
    recursive(0, n);
    t_end = clock();
    double tRec = ((double) (t_end - t_start)) / CLOCKS_PER_SEC;

//timing strcmp
    t_start = clock();
    str_cmp(string,string2);
    t_end = clock();
    double tCmp = ((double) (t_end - t_start)) / CLOCKS_PER_SEC;


//convert to nanoseconds
    int nanoFor = tFor * 1e+9;
    int nanoCmp = tCmp * 1e+9;
    int nanoRec = tRec * 1e+9;
    printf("%d, %d, %d, %d\n",n,nanoFor,nanoRec,nanoCmp);


	}

  return 0;
}

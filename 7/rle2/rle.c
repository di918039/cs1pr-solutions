#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/**
 * Encodes a string using run-length encoding.
 *
 * @param The character array on which the run-length encoding algorithm is applied.
 *
 * @return Exit status code of the function.
 */
int encode(char *);

/**
 * Decodes a string that has been encoded using run-length encoding.
 *
 * @param The character array on which the run-length decoding algorithm is applied.
 *
 * @return Exit status code of the function.
 */
int decode(char *);

/**
 * Main entrypoint to the program.
 * Encodes or decodes a given string.
 *
 * @param argc Number of arguments passed to the program.
 * @param argv Character array of command line arguments.
 *
 * @return Exit status code of the program.
 */
int main(int argc, char ** argv) {

  // If there aren't the correct number of arguments, raise an error
  if (argc != 3) {
    printf("Error: Invalid input\n");
    exit(1);
  }

  // If an empty string has been passed, raise an error
  if (strcmp(argv[2], "") == 0) {
    printf("Error: Invalid input\n");
    exit(1);
  }

  // If encoding or decoding is not specified, raise an error
  if (strcmp(argv[1], "E") != 0 && strcmp(argv[1], "D") != 0) {
    printf("Error: Invalid input\n");
    exit(1);
  }

  // Encode or decode the string that has been passed
  if (strcmp(argv[1], "E") == 0) {
    encode(argv[2]);
  } else if (strcmp(argv[1], "D") == 0) {
    decode(argv[2]);
  }

  return 0;
}

int encode(char * string) {
  int count = 0;
  char currentCharacter = string[0];
  char nextCharacter;

  // For each character in the string
  for (int i = 0; i < strlen(string); i++) {

    // Assign nextCharacter to the next character in the string
    nextCharacter = string[i+1];

    // If the current character is equal to the next character, increase the count
    if (currentCharacter == nextCharacter) {
      count++;

    // Otherwise the characters are different, so print out the current
    // character followed by its count and assign the current character
    // to the next character and reset count to 0
    } else {
      printf("%c%d", currentCharacter, count);
      currentCharacter = nextCharacter;
      count = 0;
    }

    // If the count of the current character is 10, output the character
    // followed by 9 and reset the count to 0
    if (count == 10) {
      printf("%c9", currentCharacter);
      count = 0;
    }

  }

  printf("\n");

  return 0;
}

int decode(char * string) {

  // First for loop to check if the input is valid
  for (int i = 0; i < strlen(string); i += 2) {

    // If the character is not in the alphabet or is not a blankspace, raise an error
    if ( !( (string[i] >= 'a' && string[i] <= 'z') || (string[i] >= 'A' && string[i] <= 'Z') || (string[i] == ' ') ) ) {
      printf("Error: Invalid input\n");
      exit(1);
    }

    // If the next character is not a number, raise an error
    if (!isdigit(string[i+1])) {
      printf("Error: Invalid input\n");
      exit(1);
    }

  }

  // Second for loop to iterate through each letter/blankspace in the string
  for (int i = 0; i < strlen(string); i += 2) {

    // For the number of times the letter/blankspace appears
    for (int j = string[i+1] - 48; j >= 0; j--) {

      // Print the letter/blankspace
      printf("%c", string[i]);
    }

  }

  printf("\n");

  return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

/**
 * Calculates the sum of the numbers from 1 to the given number, n, iteratively.
 *
 * @param The number from which the sum is calculated.
 *
 * @return Exit status code of the function.
 */
int iterativeSum(int);

/**
 * Calculates the sum of the numbers from 1 to the given number, n, recursively.
 *
 * @param The number from which the sum is calculated.
 *
 * @return Exit status code of the function.
 */
int recursiveSum(int);

/**
 * Checks if two strings are equal.
 * Generates two random strings of length, n, and compares them.
 *
 * @param The number used to determine the length of each string.
 *
 * @return Exit status code of the function.
 */
int stringComparison(int);

/**
 * Calculates the time taken, in seconds, for a function to execute.
 *
 * @param A pointer which points to the function that is to be executed.
 * @param The number, n.
 *
 * @return Exit status code of the function.
 */
double calculateTime(int(*function)(int), int);

/**
 * Main entrypoint to the program.
 *
 * @param Number of arguments passed to the program.
 * @param Character array of command line arguments.
 *
 * @return Exit status code of the program.
 */
int main(int argc, char ** argv) {

  // Variable used to convert seconds into nanoseconds
  int multiplier = 1e+9;

  // Declare integer array to hold the numbers passed as command line arguments
  int numbers[argc - 1];

  // For each argument passed, add to the integer array
  for (int i = 1; i < argc; i++) {
    numbers[i-1] = atoi(argv[i]);
  }

  // Calculate size of the numbers array
  int arrayLength = sizeof(numbers) / sizeof(numbers[0]);

  // Print the headings of each column
  printf("n, for loop, recursive, strcmp\n");

  // Declare variable to hold duration of function call
  double time;

  // For each number, print the elapsed time for each function call
  for (int i = 0; i < arrayLength; i++) {

    // Print the number
    printf("%d, ", numbers[i]);

    // Calculate time taken for the for loop execution and print this time
    time = calculateTime(iterativeSum, numbers[i]) * multiplier;
    printf("%g, ", time);

    // Calculate time taken for the recursion and print this time
    time = calculateTime(recursiveSum, numbers[i]) * multiplier;
    printf("%g, ", time);

    // Calculate time taken for the string comparison
    time = calculateTime(stringComparison, numbers[i]) * multiplier;
    printf("%g", time);

    printf("\n");
  }

  return 0;
}

int iterativeSum(int number) {
  int sum = 0;

  for (int i = 0; i < number; i++) {
    sum += i;
  }

  return 0;
}

int recursiveSum(int number) {

  if (number == 0) {
    return 0;
  } else {
    return number + recursiveSum(number - 1);
  }

}

int stringComparison(int number) {

  // Declare two strings of length number
  char string1[number];
  char string2[number];

  // Assign random capital letters to each string until they are full
  for (int i = 0; i < number; i++) {
    string1[i] = (rand() % 26) + 'A';
    string2[i] = (rand() % 26) + 'A';
  }

  strcmp(string1, string2);

  return 0;
}

double calculateTime(int(*function)(int), int number) {

  // Declare startTime and endTime variables
  clock_t startTime, endTime;

  // Get the current count of clock cycles
  startTime = clock();

  // Run the function
  (*function)(number);

  // Get the current count of clock cycles
  endTime = clock();

  // Calculate the duration of the function execution
  double duration = ((double) (endTime - startTime)) / CLOCKS_PER_SEC;

  // Return the duration
  return duration;

}

This repository contains the solutions for the CS1PR assignments.
They have been provided by your peers / students.

Do not give this to future students as this will impair their learning.

Thanks for the contributions of the following students:
 - Thomas Knapman hd005323
 - Student that wants to remain anonymous
 - Solutions from Julian
